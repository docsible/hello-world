<!-- DOCSIBLE START -->

# 📃 Role overview

## hello-world



Description: your role description


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 23/04/2024 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |
| Critical ⚠️            | Not available. |

### Defaults

**These are static variables with lower priority**

#### File: main.yml



| Var          | Type         | Value       |Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| [os_families](defaults/main.yml#L7)   | list   | `['Debian', 'RedHat']`  |  False  |  Operating System Families |
| [required_ram_mb](defaults/main.yml#L14)   | int   | `2048`  |  True  |  Minimum RAM in MB |
| [file_path](defaults/main.yml#L19)   | str   | `/tmp`  |  True  |  Path to File for Access Check |
| [environment_system](defaults/main.yml#L24)   | str   | `development`  |  True  |  Environment Type |





### Tasks


#### File: main.yml

| Name | Module | Has Conditions |
| ---- | ------ | --------- |
| Print a welcome message based on the operating system | ansible.builtin.debug | True |
| Gather the system facts | ansible.builtin.setup | False |
| Check if RAM is sufficient for the operation | ansible.builtin.debug | True |
| Attempt to execute potentially failing operations | block | False |
| Try to access a file that might not exist | ansible.builtin.stat | False |
| Print success message if file exists | ansible.builtin.debug | True |
| Print messages based on external variable 'environment_system' | ansible.builtin.debug | True |
| Print messages based on external variable 'environment_system' | ansible.builtin.debug | True |


## Task Flow Graphs



### Graph for main.yml

```mermaid
flowchart TD
Start
classDef block stroke:#3498db,stroke-width:2px;
classDef task stroke:#4b76bb,stroke-width:2px;
classDef include stroke:#2ecc71,stroke-width:2px;
classDef import stroke:#f39c12,stroke-width:2px;
classDef rescue stroke:#665352,stroke-width:2px;
classDef importPlaybook stroke:#9b59b6,stroke-width:2px;
classDef importTasks stroke:#34495e,stroke-width:2px;
classDef includeTasks stroke:#16a085,stroke-width:2px;
classDef importRole stroke:#699ba7,stroke-width:2px;
classDef includeRole stroke:#2980b9,stroke-width:2px;
classDef includeVars stroke:#8e44ad,stroke-width:2px;

  Start-->|Task| Print_a_welcome_message_based_on_the_operating_system0_when_ansible_os_family_in_os_families[print a welcome message based on the operating<br>system]:::task
  Print_a_welcome_message_based_on_the_operating_system0_when_ansible_os_family_in_os_families---|When: ansible os family in os families| Print_a_welcome_message_based_on_the_operating_system0_when_ansible_os_family_in_os_families
  Print_a_welcome_message_based_on_the_operating_system0_when_ansible_os_family_in_os_families-->|Task| Gather_the_system_facts1[gather the system facts]:::task
  Gather_the_system_facts1-->|Task| Check_if_RAM_is_sufficient_for_the_operation2_when_ansible_memtotal_mb___required_ram_mb[check if ram is sufficient for the operation]:::task
  Check_if_RAM_is_sufficient_for_the_operation2_when_ansible_memtotal_mb___required_ram_mb---|When: ansible memtotal mb   required ram mb| Check_if_RAM_is_sufficient_for_the_operation2_when_ansible_memtotal_mb___required_ram_mb
  Check_if_RAM_is_sufficient_for_the_operation2_when_ansible_memtotal_mb___required_ram_mb-->|Block Start| Attempt_to_execute_potentially_failing_operations3_block_start_0[[attempt to execute potentially failing operations]]:::block
  Attempt_to_execute_potentially_failing_operations3_block_start_0-->|Task| Try_to_access_a_file_that_might_not_exist0[try to access a file that might not exist]:::task
  Try_to_access_a_file_that_might_not_exist0-->|Task| Print_success_message_if_file_exists1_when_result_stat_exists[print success message if file exists]:::task
  Print_success_message_if_file_exists1_when_result_stat_exists---|When: result stat exists| Print_success_message_if_file_exists1_when_result_stat_exists
  Print_success_message_if_file_exists1_when_result_stat_exists-.->|End of Block| Attempt_to_execute_potentially_failing_operations3_block_start_0
  Print_success_message_if_file_exists1_when_result_stat_exists-->|Rescue Start| Attempt_to_execute_potentially_failing_operations3_rescue_start_0[attempt to execute potentially failing operations]:::rescue
  Attempt_to_execute_potentially_failing_operations3_rescue_start_0-->|Task| Print_error_message_if_the_file_does_not_exist0[print error message if the file does not exist]:::task
  Print_error_message_if_the_file_does_not_exist0-.->|End of Rescue Block| Attempt_to_execute_potentially_failing_operations3_block_start_0
  Print_error_message_if_the_file_does_not_exist0-->|Task| Print_messages_based_on_external_variable__environment_system_4_when_environment_system_____production_[print messages based on external variable <br>environment system ]:::task
  Print_messages_based_on_external_variable__environment_system_4_when_environment_system_____production_---|When: environment system     production | Print_messages_based_on_external_variable__environment_system_4_when_environment_system_____production_
  Print_messages_based_on_external_variable__environment_system_4_when_environment_system_____production_-->|Task| Print_messages_based_on_external_variable__environment_system_5_when_environment_system_____development_[print messages based on external variable <br>environment system ]:::task
  Print_messages_based_on_external_variable__environment_system_5_when_environment_system_____development_---|When: environment system     development | Print_messages_based_on_external_variable__environment_system_5_when_environment_system_____development_
  Print_messages_based_on_external_variable__environment_system_5_when_environment_system_____development_-->End
```


## Playbook

```yml
---
- name: test role hello world
  hosts: all
  connection: local
  roles:
    - role: ../hello-world

```
## Playbook graph
```mermaid
flowchart TD
  all-->|Role| ___hello_world[   hello world]
```

## Author Information
your name

#### License

license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version

2.1

#### Platforms

No platforms specified.
<!-- DOCSIBLE END -->